﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class drawpoints : MonoBehaviour {
    float length, width;
    public float offset = 0.2f;
    //public  int sengmentToCreateInX=4;
    [Range(0,6)]
    public int segmentToCreateInZ=4;
    Vector3 center;
    Vector3  topLeft, topRight, bottomLeft, bottomRight;
    public static List<Vector3> listPoints = new List<Vector3>();
    public float x, z,theta;
	// Use this for initialization
	void Awake () {
        length =  GetComponent<Collider>().bounds.size.x;
        width = GetComponent<Collider>().bounds.size.y;
        center = GetComponent<Collider>().bounds.center;
        x = GetComponent<MeshFilter>().sharedMesh.bounds.extents.x*transform.localScale.x;
        z = GetComponent<MeshFilter>().sharedMesh.bounds.extents.z*transform.localScale.z;
        theta = transform.rotation.eulerAngles.y;
        topRight = center + transform.forward * (z + offset) + transform.right * (x + offset);
        topLeft = center + transform.forward * (z + offset) - transform.right * (x + offset);
        bottomLeft = center - transform.forward * (z + offset) - transform.right * (x + offset);
        bottomRight = center - transform.forward * (z + offset) + transform.right * (x + offset);
        InstantiateSegments(topLeft,topRight,segmentToCreateInZ);
        InstantiateSegments(topLeft, bottomLeft, segmentToCreateInZ);
        InstantiateSegments(bottomLeft, bottomRight, segmentToCreateInZ);
        InstantiateSegments(topRight, bottomRight, segmentToCreateInZ);


        //Instantiate(transform.gameObject,topLeft,Quaternion.identity);
    }

    // Update is called once per frame
    void Update () {

       // Debug.DrawRay(topLeft, Vector3.up, Color.red);
       // Debug.DrawRay(topRight, Vector3.up, Color.green);
       //Debug.DrawRay(bottomLeft, Vector3.up, Color.blue);
        //Debug.DrawRay(bottomRight, Vector3.up, Color.yellow);
        foreach (Vector3 point in listPoints)
            Debug.DrawRay(point,Vector3.up,Color.white);
    }

    void InstantiateSegments(Vector3 pointA,Vector3 pointB,int segmentsToCreate)
    {
        //Here we calculate how many segments will fit between the two points
        //int segmentsToCreate = Mathf.RoundToInt(Vector3.Distance(pointA, pointB) / 0.5f);
        //print("segment tocreate:  "+ segmentsToCreate);
        //As we'll be using vector3.lerp we want a value between 0 and 1, and the distance value is the value we have to add
        float distance = (1f/segmentsToCreate);
        //print("distant:  "+distance);
        float lerpValue = 0;
        Vector3 instantiatePosition;
        for (int i = 0; i < segmentsToCreate; i++)
        {
            //We increase our lerpValue
            lerpValue += distance;
            //print(lerpValue);
            //Get the position
            instantiatePosition = Vector3.Lerp(pointA, pointB, lerpValue);
            if (IsAgentOnNavMesh(instantiatePosition)) { 
                listPoints.Add(instantiatePosition);
                //agent.possibleMovePoints.Add(instantiatePosition);

        }
            //Instantiate the object
           // Instantiate(ropePrefab, instantiatePosition, transform.rotation);
        }
    }
    public bool IsAgentOnNavMesh(Vector3 agentPosition)
    {

        NavMeshHit hit;

        // Check for nearest point on navmesh to agent, within onMeshThreshold
        if (NavMesh.SamplePosition(agentPosition, out hit, 1, NavMesh.AllAreas))
        {
            // Check if the positions are vertically aligned
            if (Mathf.Approximately(agentPosition.x, hit.position.x)
                && Mathf.Approximately(agentPosition.z, hit.position.z))
            {
                // Lastly, check if object is below navmesh
                return agentPosition.y >= hit.position.y;
            }
        }

        return false;
    }
}

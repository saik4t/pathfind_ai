**NPC behavior design in Unity and C#**

This is a mini project for demostrating NPC character design in Unity. The NPC hides when it is being discovered by a player. 
It hides behind obstacles dynamically, even with new obstacles added with the scene. This is a good approach for balacing load between 
design and code for a game. Since this is not fixed point based hiding, it can reduce certain manual work load for designer. 

The demostration can be found in this [Youtube Link](https://www.youtube.com/watch?v=tPgtoDrOD1o&feature=youtu.be)

---

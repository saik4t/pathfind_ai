﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour
{

    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();
    public List<Vector3> visiblePoints = new List<Vector3>();
    public List<Vector3> notVisiblePoints = new List<Vector3>();
    List<Vector3> tempList = new List<Vector3>();


    void Start()
    {
        StartCoroutine("FindTargetsWithDelay", .1f);
        
    }


    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
            FindVisibleCoverPoints();
        }
    }

    void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    visibleTargets.Add(target);
                   
                }
            }
        }
    }
    void FindVisibleCoverPoints()
    {
        //visiblePoints.Clear();
        tempList.Clear();
        foreach(Vector3 point in drawpoints.listPoints)
        {

            Vector3 dirToPoint = (point - transform.position).normalized;
            float dstToTarget = Vector3.Distance(transform.position, point);
            if (Vector3.Angle(transform.forward, dirToPoint) < viewAngle/1.8f && dstToTarget < viewRadius+2 )
            {


                if (!Physics.Raycast(transform.position, dirToPoint, dstToTarget, obstacleMask))
                {
                    if(!tempList.Contains(point))
                        tempList.Add(point);
                    if (!visiblePoints.Contains(point))
                        visiblePoints.Add(point);
                    if (notVisiblePoints.Contains(point))
                        notVisiblePoints.Remove(point);
                    
                }
            }
        }

        foreach (Vector3 point in drawpoints.listPoints)
            if (!tempList.Contains(point))
            {
                visiblePoints.Remove(point);
            notVisiblePoints.Add(point);
            }
        //notVisiblePoints.Clear();
       // notVisiblePoints = drawpoints.listPoints;
      //  foreach(Vector3 Vpoint in visiblePoints){
           // if (notVisiblePoints.Contains(Vpoint))
            //    notVisiblePoints.Remove(Vpoint);
       // }
    }


    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}

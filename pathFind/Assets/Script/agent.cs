﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]

public class agent : MonoBehaviour {
    private NavMeshAgent aiAgent;
    public bool reachedToAPoint = false;
    public bool visibleToPlayer = false;
    FieldOfView playerView;
    public  List<Vector3> possibleMovePoints = new List<Vector3>();
    public List<float> distanceList = new List<float>();
    public static Vector3[] posibleMoveArray;
    private UnityEvent m_event;
	// Use this for initialization
	void Start () {
        playerView = GameObject.FindWithTag("Player").GetComponent<FieldOfView>();
        aiAgent = GetComponent<NavMeshAgent>();
        posibleMoveArray = possibleMovePoints.ToArray();
        if (m_event == null)
            m_event = new UnityEvent();
        m_event.AddListener(gotCaught);
        StartCoroutine(updatePath());
        StartCoroutine(TryANewPosition());
	}
	
	// Update is called once per frame
	void Update () {
        if (aiAgent.remainingDistance <= aiAgent.stoppingDistance && !aiAgent.hasPath)
            reachedToAPoint = true;
        else reachedToAPoint = false;

    }
    IEnumerator updatePath(){
        float refreshRate = 0.3f;
        while(true){
            
            if (playerView.visibleTargets.Contains(transform))
                visibleToPlayer = true;
            else visibleToPlayer = false;

            //possibleMovePoints.Clear();
            foreach (Vector3 points in drawpoints.listPoints)
            {
                if(!possibleMovePoints.Contains(points))
                    possibleMovePoints.Add(points);
            }
            foreach (Vector3 points in playerView.visiblePoints)
                possibleMovePoints.Remove(points);

            // distanceList.Clear();
            //foreach(Vector3 destination in possibleMovePoints)
            // {
            //   distanceList.Add(aiAgent.re);
            //  }
           // print(aiAgent.remainingDistance);
            posibleMoveArray = possibleMovePoints.ToArray();
            yield return new WaitForSeconds(refreshRate);
            //aiAgent.SetDestination(posibleMoveArray[4]);
        }
    }

    IEnumerator TryANewPosition(){
        while (true)
        {
            
            int rand = Random.Range(0, possibleMovePoints.Count);
            if (visibleToPlayer)
                aiAgent.SetDestination(posibleMoveArray[rand]);
            if (aiAgent.remainingDistance <= aiAgent.stoppingDistance && !aiAgent.hasPath && visibleToPlayer)
            {
               // yield return new WaitForSeconds(0.2f);
              // print("check if the condition working");
            }
            yield return new WaitWhile(()=>visibleToPlayer==true || (reachedToAPoint && visibleToPlayer));
        }
    }

    void gotCaught(){
        int rand = Random.Range(0, 5);
        print(rand);
    }


}
